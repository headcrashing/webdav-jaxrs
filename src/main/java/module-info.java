/*-
 * #%L
 * WebDAV Support for JAX-RS
 * %%
 * Copyright (C) 2008 - 2024 The java.net WebDAV Project
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
module net.java.dev.webdav.jaxrs {
    requires jakarta.ws.rs;
    requires jakarta.xml.bind;

    exports net.java.dev.webdav.jaxrs;
    exports net.java.dev.webdav.jaxrs.methods;
    exports net.java.dev.webdav.jaxrs.xml;
    exports net.java.dev.webdav.jaxrs.xml.conditions;
    exports net.java.dev.webdav.jaxrs.xml.elements;
    exports net.java.dev.webdav.jaxrs.xml.properties;
    exports net.java.dev.webdav.jaxrs.search.methods;
    exports net.java.dev.webdav.jaxrs.search.xml.elements;

    opens net.java.dev.webdav.jaxrs to jakarta.xml.bind, com.sun.xml.bind;
    opens net.java.dev.webdav.jaxrs.methods to jakarta.xml.bind, com.sun.xml.bind;
    opens net.java.dev.webdav.jaxrs.xml to jakarta.xml.bind, com.sun.xml.bind;
    opens net.java.dev.webdav.jaxrs.xml.conditions to jakarta.xml.bind, com.sun.xml.bind;
    opens net.java.dev.webdav.jaxrs.xml.elements to jakarta.xml.bind, com.sun.xml.bind;
    opens net.java.dev.webdav.jaxrs.xml.properties to jakarta.xml.bind, com.sun.xml.bind;
    opens net.java.dev.webdav.jaxrs.search.methods to jakarta.xml.bind, com.sun.xml.bind;
    opens net.java.dev.webdav.jaxrs.search.xml.elements to jakarta.xml.bind, com.sun.xml.bind;
    opens net.java.dev.webdav.util to jakarta.xml.bind, com.sun.xml.bind;
}
