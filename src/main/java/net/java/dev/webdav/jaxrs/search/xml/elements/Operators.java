/*-
 * #%L
 * WebDAV Support for JAX-RS
 * %%
 * Copyright (C) 2008 - 2024 The java.net WebDAV Project
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
 // Source code is unavailable, and was generated by the Fernflower decompiler.
package net.java.dev.webdav.jaxrs.search.xml.elements;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

@XmlType(
   propOrder = {"opdescs"}
)
@XmlRootElement
public final class Operators {
   @XmlElement(
      name = "opdesc"
   )
   private List<Opdesc> opdescs;

   private Operators() {
   }

   public Operators(Opdesc... opdescs) {
      this.opdescs = opdescs == null ? null : new LinkedList(Arrays.asList(opdescs));
   }

   public final List<Opdesc> getOpdescs() {
      return this.opdescs == null ? null : Collections.unmodifiableList(this.opdescs);
   }
}
