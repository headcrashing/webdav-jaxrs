/*
 * #%L
 * WebDAV Support for JAX-RS
 * %%
 * Copyright (C) 2008 - 2024 The java.net WebDAV Project
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package net.java.dev.webdav.jaxrs.xml.elements;

import static java.util.Collections.unmodifiableList;
import static java.util.Objects.hash;
import static jakarta.xml.bind.annotation.XmlAccessType.FIELD;
import static net.java.dev.webdav.util.Utilities.array;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

import net.java.dev.webdav.jaxrs.NullArgumentException;
import net.java.dev.webdav.jaxrs.search.xml.elements.QuerySchema;
import net.java.dev.webdav.util.Utilities;

/**
 * WebDAV response XML Element.
 * 
 * @author Markus KARG (mkarg@java.net)
 * 
 * @see <a href="http://www.webdav.org/specs/rfc4918.html#ELEMENT_response">Chapter 14.24 "response XML Element" of RFC 4918
 *      "HTTP Extensions for Web Distributed Authoring and Versioning (WebDAV)"</a>
 */
@XmlAccessorType(FIELD)
@XmlType(propOrder = { "hRefs", "status", "propStats", "error", "querySchema", "responseDescription", "location" })
@XmlRootElement
public final class Response {

	@XmlElement(name = "href")
	private List<HRef> hRefs;

	private Status status;

	@XmlElement(name = "propstat")
	private List<PropStat> propStats;

	private Error error;

	@XmlElement(name = "query-schema")
	private QuerySchema querySchema;

	@XmlElement(name = "responsedescription")
	private ResponseDescription responseDescription;

	private Location location;

	@SuppressWarnings("unused")
	private Response() {
	}

	private Response(final HRef hRef, final Error error, final QuerySchema querySchema, final ResponseDescription responseDescription, final Location location) {
		if (hRef == null) {
			throw new NullArgumentException("hRef");
		} else {
			this.hRefs = new LinkedList(Collections.singletonList(hRef));
			this.error = error;
			this.querySchema = querySchema;
			this.responseDescription = responseDescription;
			this.location = location;
		}
	}

	public Response(final HRef hRef, final Error error, final ResponseDescription responseDescription, final Location location, final PropStat propStat, final PropStat... propStats) {
		this(hRef, error, (QuerySchema) null, (ResponseDescription) responseDescription, (Location) location);
		if (propStat == null) {
			throw new NullArgumentException("propStat");
		} else {
			this.propStats = new LinkedList(Collections.singletonList(propStat));
			this.propStats.addAll(Arrays.asList(propStats));
		}
	}

	/**
     * @param hRef HRef
     * @param error Error
     * @param responseDescription Response Description
     * @param location Location
     * @param propStats PropStats
	 * @deprecated Since 1.2, as the provided {@code propStats} collection is not necessarily immutable and there is no standard Java way to enforce
	 *             immutability. Use {@link #Response(HRef, Error, ResponseDescription, Location, PropStat, PropStat...)} instead.
	 * @since 1.1.1
	 */
	@Deprecated
	public Response(final HRef hRef, final Error error, final ResponseDescription responseDescription, final Location location, final Collection<PropStat> propStats) {
		this(hRef, error, (QuerySchema) null, (ResponseDescription) responseDescription, (Location) location);
		if (propStats != null && propStats.iterator().hasNext()) {
		   this.propStats = new LinkedList(propStats);
		} else {
		   throw new NullArgumentException("propStat");
		}
	 }

	public Response(final Status status, final Error error, final ResponseDescription responseDescription, final Location location, final HRef hRef, final HRef... hRefs) {
		this((HRef) hRef, (Status) status, (Error) error, (QuerySchema) null, (ResponseDescription) responseDescription, (Location) location);
		this.hRefs.addAll(Arrays.asList(hRefs));
	}

	private Response(final HRef hRef, final Status status, final Error error, final QuerySchema querySchema, final ResponseDescription responseDescription, final Location location) {
		this(hRef, error, querySchema, responseDescription, location);
		if (status == null) {
			throw new NullArgumentException("status");
		} else {
			this.status = status;
		}
	}

	public Response(final HRef hRef, final Status status, final QuerySchema querySchema, final ResponseDescription responseDescription) {
		this((HRef) hRef, (Status) status, (Error) null, (QuerySchema) querySchema, (ResponseDescription) responseDescription, (Location) null);
	}

	public final List<HRef> getHRefs() {
		return unmodifiableList(this.hRefs);
	}

	public final Status getStatus() {
		return this.status;
	}

	public final Error getError() {
		return this.error;
	}

	public final QuerySchema getQuerySchema() {
		return this.querySchema;
	}

	public final ResponseDescription getResponseDescription() {
		return this.responseDescription;
	}

	public final Location getLocation() {
		return this.location;
	}

	public final List<PropStat> getPropStats() {
		return this.propStats == null ? null : Collections.unmodifiableList(this.propStats);
	}

	@Override
	public final boolean equals(final Object o) {
		if (this == o)
			return true;

		if (!(o instanceof Response))
			return false;

		final Response that = (Response) o;

		return Arrays.equals(array(this.hRefs, this.status, this.propStats, this.error, this.responseDescription, this.location),
				array(that.hRefs, that.status, that.propStats, that.error, that.responseDescription, that.location));
	}

	@Override
	public final int hashCode() {
		return hash(this.hRefs, this.status, this.propStats, this.error, this.responseDescription, this.location);
	}

	@Override
	public final String toString() {
		return Utilities.toString(this, this.status, this.propStats, this.error, this.responseDescription, this.location);
	}
}
